﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace boekanier.Ext.ContentFinders
{
    public class HomeUrlProvider : IUrlProvider
    {
        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            var content = umbracoContext.ContentCache.GetById(id);

            // Only rewrite Url for the home node
            if (content == null || !content.DocumentTypeAlias.ToLower().Equals("home"))
            {
                return null;
            }

            var subNodeUrl = content.Parent?.Url;
            return subNodeUrl;
        }

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            return Enumerable.Empty<string>();
        }
    }
}
