﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace boekanier.Ext.ContentFinders
{
    public class WebsiteContentFinder : ContentFinderByNiceUrl, IContentFinder
    {
        public override bool TryFindContent(PublishedContentRequest contentRequest)
        {
            base.TryFindContent(contentRequest);

            var publishedContent = contentRequest.PublishedContent;

            // Only run for website Pages
            if (publishedContent == null)
            {
                return false;
            }

            switch (publishedContent.DocumentTypeAlias)
            {
                case "site":
                    // get homepicker property on first language
                    contentRequest.PublishedContent = publishedContent.Children.First().GetPropertyValue<IEnumerable<IPublishedContent>>("home").Single();
                    return true;
                case "language":
                    // get homepicker property
                    contentRequest.PublishedContent = publishedContent.GetPropertyValue<IEnumerable<IPublishedContent>>("home").Single();
                    return true;
            }


            return false;
        }
    }
}
