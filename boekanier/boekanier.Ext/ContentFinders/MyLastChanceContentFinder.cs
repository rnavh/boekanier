﻿using System;
using System.Linq;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

/// <summary>
/// Acts as the 404 content finder for the  website. If the requester url fails all other default Umbraco content finders, this one kicks in.
/// It must be registered in the application events with ContentLastChanceFinderResolver.Current.SetFinder(new MyLastChangeContentFinder());
/// Because this is the last chance content finder, it will ALWAYS return a 404. 
/// </summary>

namespace boekanier.Ext.ContentFinders
{
    public sealed class MyLastChanceContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            //* Change to true if you have subsites under your first node that need seperate 404 pages.
            const bool hasSubsitesUnderFirstNode = false;
            var contentCache = contentRequest.RoutingContext.UmbracoContext.ContentCache;

            int? rootContentId;

            if (!contentRequest.HasDomain)
            {
                // If no domain is configured take the first site (root node) and then its first child (language/home node)
                var firstRootNode = contentCache.GetAtRoot().FirstOrDefault().FirstChild();
                if (firstRootNode == null)
                {
                    LogHelper.Warn<MyLastChanceContentFinder>("No domains configured on website and no rootnode defined.");
                    return false;
                }
                rootContentId = firstRootNode.Id;
            }
            else
            {
                //* Enable one of these 2
                rootContentId = contentRequest.UmbracoDomain.RootContentId; //Umbraco 7.4 and up
                //int? rootContentId = contentRequest.Domain.RootNodeId; //Older Umbraco versions
            }


            if (rootContentId == null)
            {
                LogHelper.Debug<MyLastChanceContentFinder>("Cannot find the content associated with the domain.");
                return false;
            }
            var domainRoot = contentCache.GetById(rootContentId.Value);

            IPublishedContent root;
            if (hasSubsitesUnderFirstNode)
            {
                var segments = contentRequest.Uri.AbsolutePath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (!segments.Any())
                {
                    return false;
                }
                var firstSegment = segments.FirstOrDefault();
                root = domainRoot.Children.FirstOrDefault(x => x.UrlName == firstSegment);
                root = root ?? domainRoot.Children.First();
            }
            else
            {
                root = domainRoot;
            }

            var notfoundNode = root.FirstChild(x => x.DocumentTypeAlias == "notFound"); //* Name of the doctype alias if different from "NotFound"
            if (notfoundNode == null)
            {
                LogHelper.Warn<MyLastChanceContentFinder>("Not found page is not found on root {0}", () => root.Id);
                return false;
            }

            contentRequest.PublishedContent = notfoundNode;
            return true;
        }
    }
}