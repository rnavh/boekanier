﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using boekanier.Ext.ContentFinders;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace boekanier.Ext.StartupEvents
{
    class UmbracoAppStartup : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        { }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            LogHelper.Info<UmbracoAppStartup>("Registering HomeUrlProvider");
            UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, HomeUrlProvider>();

            LogHelper.Info<UmbracoAppStartup>("Registering WebsiteContentFinder");
            ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByNiceUrl, WebsiteContentFinder>();

            LogHelper.Info<UmbracoAppStartup>("Registering MyLastChanceContentFinder");
            ContentLastChanceFinderResolver.Current.SetFinder(new MyLastChanceContentFinder());
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        { }
    }
}
