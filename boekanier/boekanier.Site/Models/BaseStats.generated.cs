//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.7.99
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace boekanier.PublishedContentModels
{
	// Mixin content Type 1078 with alias "baseStats"
	/// <summary>Base Stats</summary>
	public partial interface IBaseStats : IPublishedContent
	{
		/// <summary>Google Analytics</summary>
		string GoogleAnalytics { get; }

		/// <summary>Tag Manager Code Body</summary>
		string TagManagerCodeBody { get; }

		/// <summary>Tag Manager Code Head</summary>
		string TagManagerCodeHead { get; }
	}

	/// <summary>Base Stats</summary>
	[PublishedContentModel("baseStats")]
	public partial class BaseStats : PublishedContentModel, IBaseStats
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "baseStats";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public BaseStats(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<BaseStats, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Google Analytics
		///</summary>
		[ImplementPropertyType("googleAnalytics")]
		public string GoogleAnalytics
		{
			get { return GetGoogleAnalytics(this); }
		}

		/// <summary>Static getter for Google Analytics</summary>
		public static string GetGoogleAnalytics(IBaseStats that) { return that.GetPropertyValue<string>("googleAnalytics"); }

		///<summary>
		/// Tag Manager Code Body: Voor instructies zie de google tag manager documentatie
		///</summary>
		[ImplementPropertyType("tagManagerCodeBody")]
		public string TagManagerCodeBody
		{
			get { return GetTagManagerCodeBody(this); }
		}

		/// <summary>Static getter for Tag Manager Code Body</summary>
		public static string GetTagManagerCodeBody(IBaseStats that) { return that.GetPropertyValue<string>("tagManagerCodeBody"); }

		///<summary>
		/// Tag Manager Code Head: Voor instructies zie de google tag manager documentatie
		///</summary>
		[ImplementPropertyType("tagManagerCodeHead")]
		public string TagManagerCodeHead
		{
			get { return GetTagManagerCodeHead(this); }
		}

		/// <summary>Static getter for Tag Manager Code Head</summary>
		public static string GetTagManagerCodeHead(IBaseStats that) { return that.GetPropertyValue<string>("tagManagerCodeHead"); }
	}
}
