﻿/// <binding AfterBuild='compile, assets, copy' Clean='clean' ProjectOpened='assets,clean, compile, watch, copy' />
"use strict";

// importeer npm packages (package.json)
var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    less = require("gulp-less"),
    sourcemaps = require('gulp-sourcemaps'),
    watch = require("gulp-watch"),
    ts = require("gulp-typescript"),
    merge = require('merge2'),
    sass = require('gulp-sass');

var paths = {
    webroot: "./"
};

paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

// gulp.watch kijkt wijzigingen na en voert dan iets uit. (laatste deel is de taak die uitgevoerd moet worden)
gulp.task("watch", function () {
    gulp.watch(["./Assets/Styles/*.less"], ["less"]);
    gulp.watch(["./Assets/Scripts/*.js"], ["assets:js"]);
    gulp.watch(["./Assets/Fonts/*"], ["assets:fonts"]);
	gulp.watch(["./Assets/Scripts/*.ts"], ["typescript"]);
});

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});


gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("copy:js",
    function () {
        console.log("copy JS files");
        return gulp
            .src([
				// ResponsiveBP
                //"./node_modules/responsive-bp/build/responsive.js",
				
				// Jquery
                //"./node_modules/jquery/dist/jquery.min.js"
            ])
            .pipe(gulp.dest("./scripts"));
    });
gulp.task("copy:css",
    function () {
        console.log("copy CSS files");

        return gulp
            .src([
				// ResponsiveBP
                // "./node_modules/responsive-bp/build/responsive.min.css",
            ])
            .pipe(gulp.dest("./css"));
    });

gulp.task("copy:fonts",
    function () {
        console.log("copy Font files");

        return gulp
            .src([
				// Font-Awesome
                //"./node_modules/font-awesome/fonts/fontawesome-webfont.eot",
                //"./node_modules/font-awesome/fonts/fontawesome-webfont.svg",
                //"./node_modules/font-awesome/fonts/fontawesome-webfont.ttf",
                //"./node_modules/font-awesome/fonts/fontawesome-webfont.woff",
                //"./node_modules/font-awesome/fonts/fontawesome-webfont.woff2",
                //"./node_modules/font-awesome/fonts/FontAwesome.otf"
            ])
            .pipe(gulp.dest("./Fonts"));
    });

gulp.task("copy", ["copy:js", "copy:css", "copy:fonts"]);
gulp.task("compile", ["less", "sass", "typescript"]);
gulp.task("assets", ["assets:js", "assets:fonts"]);

gulp.task("assets:js", function () {
    console.log("copy some JS to the right folder");
    return gulp.src(["./assets/Scripts/*.js"])
        .pipe(gulp.dest("./scripts"));
});

gulp.task("assets:fonts", function () {
    console.log("copy some Fonts to the right folder");
    return gulp.src(["./assets/Fonts/*"])
        .pipe(gulp.dest("./Fonts"));
});


gulp.task('less', function () {
    return gulp.src('./assets/Styles/*.less')
        .pipe(sourcemaps.init())
        .pipe(less({
            // plugins: [autoprefix]
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css'));
});

gulp.task('sass', function () {
    return gulp.src('./Assets/Styles/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('typescript', function () {
    var tsResult = gulp.src('./assets/Scripts/*.ts')
        .pipe(ts());

    return merge([
        tsResult.js.pipe(gulp.dest('./Scripts'))
    ]);
});